Board Data Warehouse Controller
===============================

Система управления хранилищем данных. Основная задача - хранять данные полученные по каналу MQTT.


Как это работает на данный момент:
----------------------------------

1. Через канал MQTT подписывается на тему(topic): "board/+/sensors/+"
2. Полученные сигналы записывает в файла, который находится в директории "path/from/config/" + "topic"


Установка:
----------

Качаем репозиторий:

```bash
$ git clone https://gitlab.com/kfuvmkteam/board_data_warehouse.git
```

После нужно будет установить виртуальное окружение (можно почитать [здесь](http://adw0rd.com/2012/6/19/python-virtualenv/)). 
И поставить все используемые модули. Cписок находится в файле requirements.txt

```
$ cd board_data_warehouse/
$ pip install -r requirements.txt
```

Потом в config.yaml настраиваем доступ к MQTT хосту и указываем путь, где хранить данные.

Чтобы запустить бота:

```
$ python worker.py
```


TODO
----
* Поддержка конфигов
* Синхронизацию Хранилища данных с БД других агентов, например c [BoardBot](https://gitlab.com/kfuvmkteam/boardbot)
* Дописать README


Контакты:
---------

Ramil Gataullin: ramil.gata@gmail.com