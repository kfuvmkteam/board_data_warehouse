# -*- coding: UTF-8 -*-
"""
    First version of data warehouse controller
"""

import os
import logging
import datetime

import yaml
import paho.mqtt.client as mqtt

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    # filename='log.log',
                    level=logging.INFO)


def loadconfig(filename):
    with open(filename, 'r') as stream:
        configs = yaml.load(stream)
    return configs

CONFIGFILE = "config.yaml"
CONFIGS = loadconfig(CONFIGFILE)
PATH = CONFIGS['DATAPATH']

def write_value(topic, value):
    directory = os.path.dirname(os.path.join(PATH, topic))
    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(os.path.join(PATH, topic), 'a') as stream:
        data = '%s\t%s\n' % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), value)
        stream.write(data.encode('utf-8'))

def on_connect(client, userdata, flags, rc):
    logging.info("Connected with result code %s", str(rc))
    client.subscribe("board/+/sensors/+")

def on_message(client, userdata, msg):
    logging.info("***** Got message from '%s': '%s'", msg.topic, str(msg.payload))
    try:
        write_value(msg.topic, int(msg.payload))
    except Exception as err:
        logging.error("Error on parsing message: %s", err)

def main():
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set(CONFIGS['MQTT_USERNAME'], CONFIGS['MQTT_PASSWORD'])
    client.connect(CONFIGS['MQTT_HOST'], CONFIGS['MQTT_PORT'], 60)
    client.loop_forever()

if __name__ == '__main__':
    main()
